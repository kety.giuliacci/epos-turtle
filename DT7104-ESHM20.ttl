#DT7104 - ESHM20

@prefix adms: <http://www.w3.org/ns/adms#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix epos: <https://www.epos-eu.org/epos-dcat-ap#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix hydra: <http://www.w3.org/ns/hydra/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix schema: <http://schema.org/> .
@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix cnt: <http://www.w3.org/2011/content#> .
@prefix locn: <http://www.w3.org/ns/locn#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix http: <http://www.w3.org/2006/http#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix gsp: <http://www.opengis.net/ont/geosparql#> .
@prefix dqv: <http://www.w3.org/ns/dqv#> .
@prefix oa: <http://www.w3.org/ns/oa#> .

### Dataset

<https://www.epos-eu.org/epos-dcat-ap/Seismology/Dataset/EFEHR/hazardmaps/hmap1846> a dcat:Dataset;
	dct:identifier "https://www.epos-eu.org/epos-dcat-ap/Seismology/Dataset/EFEHR/hazardmaps/hmap1846" ;
	#Unique ID
    adms:identifier [ a adms:Identifier ;
            adms:schemeAgency "DOI" ;
            skos:notation "https://doi.org/10.12686/a15" ;

    ];
	adms:identifier [ a adms:Identifier ;
            adms:schemeAgency "DT-GEO" ;
            skos:notation "DT7104" ;
    ];

	#Name
	dct:title "Seismogenic Source Model of the 2020 European Seismic Hazard Model (ESHM20)" ;

	# Example of frequency using a controlled vocabulary http://dublincore.org/2012/06/14/dctype
	dct:type "http://purl.org/dc/dcmitype/Dataset"^^xsd:anyURI;

	#Keywords
	dcat:keyword "Seismogenic Source Model", "Active Faults", "Area Sources", "Smoothed Seismicity", "European Seismic Hazard Model (ESHM20)";

	#Description
	dct:description "The seismogenic source model of ESHM20 is built upon the foundation of the ESHM13 enhanced with newly developed local and regional seismogenic sources. The main seismogenic source model consists of four distinct source models aimed at capturing the spatial and temporal uncertainty of the earthquake rate forecast across the entire pan-European region: area sources, hybrid active faults and background seismicity, subduction sources and deep seismicity sources.";

	dcat:distribution <Seismology/EFEHR/HazardMap/Distribution/WMS/hmap1846> ;
	#Version
	dcat:version "";

	#Spatial relevance 80N32W, 80N50E; 25N32W,25N50E
	dct:spatial [ a dct:Location;
		locn:geometry "POLYGON((32 25, 32 80, 50 80, 50 25,32 25))"^^gsp:wktLiteral ;
	];

	#Temporal relevance
	#dct:temporal [ a dct:PeriodOfTime;
		#schema:startDate "2006-01-01T00:00:00Z"^^xsd:dateTime;
		#schema:endDate "YYYY-MM-DDThh:mm:ssZ"^^xsd:dateTime;
	#];

	#Organisation
	dct:publisher <PIC:999979015> ;

	#Person role
	dcat:contactPoint <https://orcid.org/0000-0003-4086-8755/author> ;

	#Security constraints - Security of data storage -Security of data transfer- Privacy constraints - Contribution policies
	#dct:accessRights "";

	#Curation and provenance obligations
	#dct:provenance  "";

	#Related publications
	dct:isReferencedBy "https://doi.org/10.12686/a15";

	#Additional metadata
	#dcat:landingPage "";

	#Quality assurance
	dqv:hasQualityAnnotation [ a oa:Annotation ;
		oa:hasBody "http://hazard.efehr.org/en/web-services/qa"^^xsd:anyURI ;
	];
	#Project ID
	#dct:isReferencedBy

    #Periodicity 
	dct:accrualPeriodicity "http://purl.org/cld/freq/irregular"^^xsd:anyURI ;
    dcat:theme  <category:DTGEO_conc> ;
.

### Distribution

<Seismology/EFEHR/HazardMap/Distribution/WMS/hmap1846> a dcat:Distribution ;
	dcat:accessURL "https://gitlab.seismo.ethz.ch/efehr/eshm20"^^xsd:anyURI;
	dct:identifier "Seismology/EFEHR/HazardMap/Distribution/WMS/hmap1846" ;
	dct:title "Seismogenic Source Model of the 2020 European Seismic Hazard Model (ESHM20)" ;

	#Description
	dct:description "The seismogenic source model of ESHM20 is built upon the foundation of the ESHM13 enhanced with newly developed local and regional seismogenic sources. The main seismogenic source model consists of four distinct source models aimed at capturing the spatial and temporal uncertainty of the earthquake rate forecast across the entire pan-European region: area sources, hybrid active faults and background seismicity, subduction sources and deep seismicity sources.";

	#File format https://www.iana.org/assignments/media-types/media-types.xhtml
	#-->shapefiles, xml
	dct:format "shapefiles"^^xsd:anyURI;
	dcat:mediaType "https://www.iana.org/assignments/media-types/application/xml"^^xsd:anyURI;

	#URL
	dcat:downloadURL "https://gitlab.seismo.ethz.ch/efehr/eshm20"^^xsd:anyURI; #for downloadable file

	#dcat:accessService <>;#DataService that gives access to the Dataset

	#Maturity level
	# adms:status

	#Licensing URL - #Licensing constraints (to be renamed to #“Licensing ID”)
	#https://spdx.org/licenses
	dct:license "https://spdx.org/licenses/CC-BY-4.0.html"^^xsd:anyURI;
	
	#minSTORMB
	#dcat:byteSize "";
.

### Organization

<PIC:999979015> a schema:Organization;
  schema:identifier [ a schema:PropertyValue;
    schema:propertyID "PIC";
    schema:value "999979015";
  ];
  schema:legalName "ETH Zurich";
  schema:address [ a schema:PostalAddress;
    schema:streetAddress "";
    schema:addressLocality "Zürich";
    schema:postalCode "";
    #schema:addressCountry "Switzerland";
    schema:addressCountry "CH";
  ];
  #schema:logo "";
  schema:url "http://www.seismo.ethz.ch"^^xsd:anyURI;
  schema:email "info@sed.ethz.ch";
  schema:contactPoint <https://orcid.org/0000-0003-4086-8755/author>;
.


### Person

<https://orcid.org/0000-0003-4086-8755> a schema:Person;
#Person ID
	schema:identifier [ a schema:PropertyValue ;
		schema:propertyID "orcid" ;
		schema:value "0000-0003-4086-8755" ;
	];
	#Person name
	schema:givenName "Laurentiu";
	schema:familyName "Danciu";

	schema:contactPoint <https://orcid.org/0000-0003-4086-8755/author> ;
	schema:email "laurentiu.danciu@sed.ethz.ch";
	schema:url "https://orcid.org/0000-0003-4086-8755"^^xsd:anyURI ;
.

<https://orcid.org/0000-0003-4086-8755/author> a schema:ContactPoint ;
	#Person email
	schema:email "laurentiu.danciu@sed.ethz.ch";
	#Person role
	schema:contactType "author";
	schema:availableLanguage "en" ;
.
