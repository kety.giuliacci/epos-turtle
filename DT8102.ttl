#DT8102 - COOPER_BASIN

@prefix adms: <http://www.w3.org/ns/adms#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix epos: <https://www.epos-eu.org/epos-dcat-ap#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix hydra: <http://www.w3.org/ns/hydra/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix schema: <http://schema.org/> .
@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix cnt: <http://www.w3.org/2011/content#> .
@prefix locn: <http://www.w3.org/ns/locn#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix http: <http://www.w3.org/2006/http#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix gsp: <http://www.opengis.net/ont/geosparql#> .
@prefix dqv: <http://www.w3.org/ns/dqv#> .
@prefix oa: <http://www.w3.org/ns/oa#> .

### Dataset

<anthropogenic_hazards/dataset/episode/COOPER_BASIN> a dcat:Dataset;
	dct:identifier "anthropogenic_hazards/dataset/episode/COOPER_BASIN";
	#Unique ID 
	adms:identifier [ a adms:Identifier ;
            adms:schemeAgency "DOI" ;
            skos:notation "https://doi.org/10.25171/InstGeoph_PAS_ISEPOS-2020-001" ;

    ];
	adms:identifier [ a adms:Identifier ;
            adms:schemeAgency "DT-GEO" ;
            skos:notation "DT8102" ;
    ]; 

	#Name
	dct:title "COOPER BASIN: geothermal energy production" ;

	# Example of frequency using a controlled vocabulary http://dublincore.org/2012/06/14/dctype
	dct:type "http://purl.org/dc/dcmitype/Dataset"^^xsd:anyURI;

	#Keywords
	dcat:keyword "anthropogenic hazards", "TCS AH", "EPISODES Platform", "episode", "geothermal energy production", "anthropogenic seismicity", "catalogue", "injection industrial data";
	
	#Description
	dct:description "The Cooper Basin geothermal field is located in the northeast of South Australia near the Queensland border. Geothermal exploration started in 2002, and to date six deep wells have been drilled into the granite to a depth level of 3629–4852 m. Four of these wells are located in the Habanero field, the other two wells are at distances of 9 and 18 km, respectively, in the Jolokia and Savina fields. Several hydraulic stimulations were conducted to enhance the hydraulic conductivity in the subsurface. Stimulation activities in the Habanero field were accompanied by pronounced seismic activity occurring on a single, subhorizontal fault, which existed prior to geothermal exploration (Baisch et al., 2006, 2009). The Habanero 4 well was drilled in the year 2012 to a total depth of 4204 m. It intersects the previously stimulated reservoir at a depth level of 4160 m, which is within 20 m of the predicted intersection depth based on previous seismicity (Baisch et al., 2006, 2009). The data provided here refers to the stimulation of Habanero#4. Following previous stimulations in the Habanero reservoir, Habanero 4 was hydraulically stimulated by injecting a total quantity of 34,000 m3 of water starting on 14 November 2012. Maximum flow rates exceeded 60 L/s, and the wellhead pressure reached peak values in the order of 50 MPa. Previous hydraulic testing of the reservoir revealed an extremely high artesian pressure of about 35 MPa above hydrostatic (Baisch et al., 2009). Immediately before the injection, a slightly lower overpressure of about 34 MPa was observed. For monitoring the stimulation of Habanero 4, a 24-station network was installed. The network includes the permanent stations installed around the Habanero, Jolokia, and Savina wellsites, consisting of three-component geophones deployed in boreholes at depth levels of 78–370 m and one surface seismometer (Baisch et al., 2015). Additionally, nine stations equipped with three-component 1 Hz surface seismometers were temporarily deployed to improve the coverage to the east and northwest, respectively. Each seismic station was equipped with a three-channel 24-bit digitizer recording continuously at a sampling rate of 500 Hz. Continuous waveform recordings were stored locally on a hard disk, and the seven live stations additionally transmitted data by a WLAN to the central data acquisition office located at the Habanero campsite. Literature list: 1. Baisch, Stefan and Weidler, R. and Vörös, Robert and Wyborn, D. and de Graaf, L. (2006) Induced Seismicity during the Stimulation of a Geothermal HFR Reservoir in the Cooper Basin, Australia. Bulletin of the Seismological Society of America, 96 (6). pp. 2242-2256. 2. Baisch, Stefan and Vörös, Robert and Weidler, R. and Wyborn, D. (2009) Investigation of Fault Mechanisms during Geothermal Reservoir Stimulation Experiments in the Cooper Basin, Australia. Bulletin of the Seismological Society of America, 99 (1). pp. 148-158. 3. Baisch, Stefan and Rothert, Elmar and Stang, Henrik and Vörös, Robert and Koch, Christopher and McMahon, Andrew (2015) Continued Geothermal Reservoir Stimulation Experiments in the Cooper Basin (Australia). Bulletin of the Seismological Society of America, 105 (1). pp. 198-209. Episode integrated in the framework of: S4CE, Science for Clean Energy. This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 764810. How to cite: IS EPOS (2020), Episode: COOPER BASIN, https://tcs.ah-epos.eu/#episode:COOPER_BASIN, doi:10.25171/InstGeoph_PAS_ISEPOS-2020-001";

	dcat:distribution <anthropogenic_hazards/distribution/episode-elements/COOPER_BASIN> ;

	#Version
	dcat:version "https://doi.org/10.25171/InstGeoph_PAS_ISEPOS-2020-001";
	#Spatial relevance WGS-84: -27.8157, 140.7544
	dct:spatial [ a dct:Location;
		locn:geometry "POINT(140.7544 -27.8157)"^^gsp:wktLiteral ;
	];
	#Temporal relevance
	dct:temporal [ a dct:PeriodOfTime;
		schema:startDate "2012-11-10T12:21:28Z"^^xsd:dateTime;
		#schema:endDate "2013-02-26T12:53:19Z"^^xsd:dateTime;
	];
	#Organisation
	dct:publisher <PIC:916003593> ;

	#Person role
	dcat:contactPoint <https://orcid.org/0000-0002-1831-8960/contactPoint> ;

	#Security constraints - Security of data storage -Security of data transfer- Privacy constraints - Contribution policies
	dct:accessRights  "Security constraints:Public Read access. Download for registered users. Create, Udate, Delete only for data admins. - Security of data storage:Backup (daily snapshots) -Security of data transfer:SSL- Privacy constraints: Literature list - Contribution policies: Record of all activity (whom, what, when). Versioning of metadata and individual data";

	#Curation and provenance obligations
	dct:provenance "Record of all activity (whom, what, when). Versioning of metadata and individual data"; 
    
	#Related publications
	dct:isReferencedBy "https://episodesplatform.eu/eprints/view/divisions/IP13.html";

	#Additional metadata
	dcat:landingPage "https://episodesplatform.eu/api/epos/episodes?episode=COOPER_BASIN";

	#Quality assurance
	dqv:hasQualityAnnotation [ a oa:Annotation ;
		oa:hasBody "https://docs.cyfronet.pl/display/ISDOC/Data+Quality+Assurance "^^xsd:anyURI ;
	];

	#Project ID
	dct:isReferencedBy "DTC81";
    dcat:theme  <category:DTGEO_conc> ;
.

### Distribution

 <anthropogenic_hazards/distribution/episode-elements/COOPER_BASIN> a dcat:Distribution ;
	dcat:accessURL ""^^xsd:anyURI;
	dct:identifier "anthropogenic_hazards/distribution/episode-elements/COOPER_BASIN" ;
	dct:title "COOPER BASIN: geothermal energy production" ;

	#Description
	dct:description "The Cooper Basin geothermal field is located in the northeast of South Australia near the Queensland border. Geothermal exploration started in 2002, and to date six deep wells have been drilled into the granite to a depth level of 3629–4852 m. Four of these wells are located in the Habanero field, the other two wells are at distances of 9 and 18 km, respectively, in the Jolokia and Savina fields. Several hydraulic stimulations were conducted to enhance the hydraulic conductivity in the subsurface. Stimulation activities in the Habanero field were accompanied by pronounced seismic activity occurring on a single, subhorizontal fault, which existed prior to geothermal exploration (Baisch et al., 2006, 2009). The Habanero 4 well was drilled in the year 2012 to a total depth of 4204 m. It intersects the previously stimulated reservoir at a depth level of 4160 m, which is within 20 m of the predicted intersection depth based on previous seismicity (Baisch et al., 2006, 2009). The data provided here refers to the stimulation of Habanero#4. Following previous stimulations in the Habanero reservoir, Habanero 4 was hydraulically stimulated by injecting a total quantity of 34,000 m3 of water starting on 14 November 2012. Maximum flow rates exceeded 60 L/s, and the wellhead pressure reached peak values in the order of 50 MPa. Previous hydraulic testing of the reservoir revealed an extremely high artesian pressure of about 35 MPa above hydrostatic (Baisch et al., 2009). Immediately before the injection, a slightly lower overpressure of about 34 MPa was observed. For monitoring the stimulation of Habanero 4, a 24-station network was installed. The network includes the permanent stations installed around the Habanero, Jolokia, and Savina wellsites, consisting of three-component geophones deployed in boreholes at depth levels of 78–370 m and one surface seismometer (Baisch et al., 2015). Additionally, nine stations equipped with three-component 1 Hz surface seismometers were temporarily deployed to improve the coverage to the east and northwest, respectively. Each seismic station was equipped with a three-channel 24-bit digitizer recording continuously at a sampling rate of 500 Hz. Continuous waveform recordings were stored locally on a hard disk, and the seven live stations additionally transmitted data by a WLAN to the central data acquisition office located at the Habanero campsite. Literature list: 1. Baisch, Stefan and Weidler, R. and Vörös, Robert and Wyborn, D. and de Graaf, L. (2006) Induced Seismicity during the Stimulation of a Geothermal HFR Reservoir in the Cooper Basin, Australia. Bulletin of the Seismological Society of America, 96 (6). pp. 2242-2256. 2. Baisch, Stefan and Vörös, Robert and Weidler, R. and Wyborn, D. (2009) Investigation of Fault Mechanisms during Geothermal Reservoir Stimulation Experiments in the Cooper Basin, Australia. Bulletin of the Seismological Society of America, 99 (1). pp. 148-158. 3. Baisch, Stefan and Rothert, Elmar and Stang, Henrik and Vörös, Robert and Koch, Christopher and McMahon, Andrew (2015) Continued Geothermal Reservoir Stimulation Experiments in the Cooper Basin (Australia). Bulletin of the Seismological Society of America, 105 (1). pp. 198-209. Episode integrated in the framework of: S4CE, Science for Clean Energy. This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 764810. How to cite: IS EPOS (2020), Episode: COOPER BASIN, https://tcs.ah-epos.eu/#episode:COOPER_BASIN, doi:10.25171/InstGeoph_PAS_ISEPOS-2020-001";

	#File format https://www.iana.org/assignments/media-types/media-types.xhtml
	dct:format "https://www.iana.org/assignments/media-types/application/vnd.fdsn.mseed"^^xsd:anyURI;
	dcat:mediaType "https://www.iana.org/assignments/media-types/application/xml"^^xsd:anyURI;

	#URL
	dcat:downloadURL "https://episodesplatform.eu/?lang=en#episode:COOPER_BASIN"^^xsd:anyURI; #for downloadable file
	#dcat:accessService <>;#DataService that gives access to the Dataset

	#Maturity level
	# adms:status

	#Licensing URL - #Licensing constraints (to be renamed to #“Licensing ID”)
	#https://spdx.org/licenses
	dct:license "https://spdx.org/licenses/CC-BY-4.0.html"^^xsd:anyURI;

	#minSTORMB
	dcat:byteSize "MinStorMB 89 600 MB-maxSTORMB 89 600 MB";
.

### Organization

<PIC:916003593> a schema:Organization;
  schema:identifier [ a schema:PropertyValue;
    schema:propertyID "pic";
    schema:value "916003593";
  ];
  schema:legalName "Q-con GmbH";
  schema:address [ a schema:PostalAddress;
    schema:streetAddress "Markstrasse 39";
    schema:addressLocality "Bad Bergzabern";
    schema:postalCode "76887";
    #schema:addressCountry "Germany";  
    schema:addressCountry "DE";
  ];
  schema:url "https://www.q-con.de/"^^xsd:anyURI;
.

### Person

<http://orcid.org/0000-0002-1831-8960> a schema:Person;
    schema:identifier [ a schema:PropertyValue;
        schema:propertyID "orcid";
        schema:value "0000-0002-1831-8960";
    ];
    schema:familyName "Baisch";
    schema:givenName "Stefan";
    schema:address [ a schema:PostalAddress;
        schema:streetAddress "Markstrasse 39";
        schema:addressLocality "Bad Bergzabern";
        schema:postalCode "76887";
        schema:addressCountry "Germany";
    ];
    schema:email "baisch@q-con.de";
    schema:affiliation <PIC:916003593>;
    schema:contactPoint <http://orcid.org/0000-0002-1831-8960/contactPoint>;
. 

### ContactPoint

<http://orcid.org/0000-0002-1831-8960/contactPoint> a schema:ContactPoint ;
	#Person email
	schema:email "baisch@q-con.de";
	#Person role
	schema:contactType "contactPoint";
	schema:availableLanguage "en" ;
.
