#DT7105 - ESRM20

@prefix adms: <http://www.w3.org/ns/adms#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix epos: <https://www.epos-eu.org/epos-dcat-ap#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix hydra: <http://www.w3.org/ns/hydra/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix schema: <http://schema.org/> .
@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix cnt: <http://www.w3.org/2011/content#> .
@prefix locn: <http://www.w3.org/ns/locn#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix http: <http://www.w3.org/2006/http#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix gsp: <http://www.opengis.net/ont/geosparql#> .
@prefix dqv: <http://www.w3.org/ns/dqv#> .
@prefix oa: <http://www.w3.org/ns/oa#> .

### Dataset

<https://www.epos-eu.org/epos-dcat-ap/Seismology/Dataset/EFEHR/EU_SeismicRiskIndex> a dcat:Dataset;
	dct:identifier "https://www.epos-eu.org/epos-dcat-ap/Seismology/Dataset/EFEHR/EU_SeismicRiskIndex"; #existing in EPOS ICS-C prod ???
	#Unique ID
    adms:identifier [ a adms:Identifier ;
            adms:schemeAgency "DOI" ;
            skos:notation "https://doi.org/10.7414/EUC-ESRM20-RISK-INDEX-VIEWER" ;

    ];
	adms:identifier [ a adms:Identifier ;
            adms:schemeAgency "DT-GEO" ;
            skos:notation "DT7105" ;
    ];

	#Name
	dct:title "Input files for computing the 2020 European Seismic Risk Model (ESRM20)" ;

	# Example of frequency using a controlled vocabulary http://dublincore.org/2012/06/14/dctype
	dct:type "http://purl.org/dc/dcmitype/Dataset"^^xsd:anyURI;

	#Keywords
	dcat:keyword "Exposure", "vulnerability", "site amplification", "2020 European Seismic Risk Model";
	
	#Description
	dct:description "OpenQuake-engine input files for reproducibility of the ESRM20";

	dcat:distribution <https://www.epos-eu.org/epos-dcat-ap/Seismology/Distribution/EFEHR/OGC/WMS/EU_SeismicRiskIndex> ;

	#Version
	dcat:version "";
	#Spatial relevance 80N32W, 80N50E; 25N32W,25N50E
	dct:spatial [ a dct:Location;
		locn:geometry "POLYGON((32 25, 32 80, 50 80, 50 25,32 25))"^^gsp:wktLiteral ;
	];
	#Temporal relevance
	#dct:temporal [ a dct:PeriodOfTime;
		#schema:startDate "2006-01-01T00:00:00Z"^^xsd:dateTime;
		#schema:endDate "YYYY-MM-DDThh:mm:ssZ"^^xsd:dateTime;
	#];
	#Organisation
	dct:publisher <PIC:999844476> ;

	#Person role
	dcat:contactPoint <http://orcid.org/0000-0002-1216-1245/author> ;

	#Security constraints - Security of data storage -Security of data transfer- Privacy constraints - Contribution policies
	#dct:accessRights "";

	#Curation and provenance obligations
	#dct:provenance  "";

	#Related publications
	dct:isReferencedBy "https://doi.org/10.7414/EUC-EFEHR-TR002-ESRM20";

	#Additional metadata
	#dcat:landingPage "";

	#Quality assurance
	dqv:hasQualityAnnotation [ a oa:Annotation ;
		oa:hasBody "http://risk.efehr.org/qa"^^xsd:anyURI ;
	];

	#Project ID
	#dct:isReferencedBy
	
    #Periodicity
	dct:accrualPeriodicity "http://purl.org/cld/freq/irregular"^^xsd:anyURI ;
    dcat:theme  <category:DTGEO_conc> ;
.

### Distribution

 <https://www.epos-eu.org/epos-dcat-ap/Seismology/Distribution/EFEHR/OGC/WMS/EU_SeismicRiskIndex> a dcat:Distribution ;
	dcat:accessURL "https://gitlab.seismo.ethz.ch/efehr/esrm20"^^xsd:anyURI;
	dct:identifier "https://www.epos-eu.org/epos-dcat-ap/Seismology/Distribution/EFEHR/OGC/WMS/EU_SeismicRiskIndex" ;
	dct:title "Input files for computing the 2020 European Seismic Risk Model (ESRM20)" ;

	#Description
	dct:description "OpenQuake-engine input files for reproducibility of the ESRM20";

	#File format https://www.iana.org/assignments/media-types/media-types.xhtml
	#-->shapefiles, xml
	dct:format "https://www.iana.org/assignments/media-types/application/csv"^^xsd:anyURI;
	dcat:mediaType "https://www.iana.org/assignments/media-types/application/xml"^^xsd:anyURI;

	#URL
	dcat:downloadURL "https://gitlab.seismo.ethz.ch/efehr/esrm20"^^xsd:anyURI; #for downloadable file
	#dcat:accessService <>;#DataService that gives access to the Dataset

	#Maturity level
	# adms:status

	#Licensing URL - #Licensing constraints (to be renamed to #“Licensing ID”)
	#https://spdx.org/licenses
	dct:license "https://spdx.org/licenses/CC-BY-4.0.html"^^xsd:anyURI;

	#minSTORMB
	#dcat:byteSize "";
.

### Organization

<PIC:999844476> a schema:Organization;
	schema:identifier [ a schema:PropertyValue;
		schema:propertyID "PIC";
		schema:value "999844476";
	];
	schema:legalName "Fondazione EUCENTRE";
	schema:address [ a schema:PostalAddress;
		schema:streetAddress "Via Ferrata, 1";
		schema:addressLocality "Pavia";
		schema:postalCode "27100";
		#schema:addressCountry "Italy"; 
		schema:addressCountry "IT";
	];
	schema:logo "https://www.eucentre.it/wp-content/uploads/2018/03/logo_eucentre_100px.png"^^xsd:anyURI;
	schema:url "http://www.eucentre.it/"^^xsd:anyURI;
	schema:email "info@eucentre.it";
	schema:telephone "+3903825169811";
	schema:contactPoint <http://orcid.org/0000-0002-1216-1245/author> ;
.

### Person

<http://orcid.org/0000-0002-1216-1245> a schema:Person;
	schema:identifier [ a schema:PropertyValue;
		schema:propertyID "orcid";
		schema:value "0000-0002-1216-1245";
	];
	schema:familyName "Crowley";
	schema:givenName "Helen";
	schema:address [ a schema:PostalAddress;
		schema:streetAddress "Via Ferrata, 1";
		schema:addressLocality "Pavia";
		schema:postalCode "27100";
		schema:addressCountry "Italy";
	];
	schema:email "helen.crowley@eucentre.it";
	schema:telephone "+3903825169811";
	schema:qualifications "Researcher";
	schema:affiliation <PIC:999844476>;
	schema:contactPoint <http://orcid.org/0000-0002-1216-1245/author> ;
.

### ContactPoint

<http://orcid.org/0000-0002-1216-1245/author> a schema:ContactPoint ;
	#Person email
	schema:email "helen.crowley@eucentre.it";
	#Person role
	schema:contactType "author";
	schema:availableLanguage "en" ;
.
