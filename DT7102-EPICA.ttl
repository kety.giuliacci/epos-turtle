#DT7102 - EPICA

@prefix adms: <http://www.w3.org/ns/adms#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix epos: <https://www.epos-eu.org/epos-dcat-ap#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix vcard: <http://www.w3.org/2006/vcard/ns#> .
@prefix hydra: <http://www.w3.org/ns/hydra/core#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix schema: <http://schema.org/> .
@prefix dcat: <http://www.w3.org/ns/dcat#> .
@prefix cnt: <http://www.w3.org/2011/content#> .
@prefix locn: <http://www.w3.org/ns/locn#> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix http: <http://www.w3.org/2006/http#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix gsp: <http://www.opengis.net/ont/geosparql#> .
@prefix dqv: <http://www.w3.org/ns/dqv#> .
@prefix oa: <http://www.w3.org/ns/oa#> .

### Dataset

<https://www.epos-eu.org/epos-dcat-ap/Seismology/Dataset/AHEAD/events/EPICA> a dcat:Dataset;
	dct:identifier "https://www.epos-eu.org/epos-dcat-ap/Seismology/Dataset/AHEAD/events/EPICA";
	#Unique ID
    adms:identifier [ a adms:Identifier ;
            adms:schemeAgency "DOI" ;
            skos:notation "https://doi.org/10.13127/epica.1.1" ;

    ];
	adms:identifier [ a adms:Identifier ;
            adms:schemeAgency "DT-GEO" ;
            skos:notation "DT7102" ;
    ];


	#Name
	dct:title "European PreInstrumental Earthquake Catalogue (EPICA)" ;

	# Example of frequency using a controlled vocabulary http://dublincore.org/2012/06/14/dctype
	dct:type "http://purl.org/dc/dcmitype/Dataset"^^xsd:anyURI;

	#Keywords
	dcat:keyword "European Pre-instrumental Earthquake Catalogue", "European Historical Earthquake Catalogue", "Unified Earthquake Catalogue", "European Seismic Hazard Model (ESHM20)";

	#Description
	dct:description "The European Pre-Instrumental Earthquake Catalogue (EPICA) is the 1000-1899 seismic catalogue compiled within the framework of the SERA (Seismology and Earthquake Engineering Research Infrastructure Alliance for Europe) Horizon 2020 project for the European Seismic Hazard Model 2020 (ESHM20).";

	dcat:distribution <https://www.epos-eu.org/epos-dcat-ap/Seismology/Dataset/AHEAD/events/EPICA/distribution/OGC/WMS> ;
	#Version
	dcat:version "3.3";

	#Spatial relevance 80N32W, 80N50E; 25N32W,25N50E
	dct:spatial [ a dct:Location;
		locn:geometry "POLYGON((32 25, 32 80, 50 80, 50 25,32 25))"^^gsp:wktLiteral ;
	];

	#Temporal relevance
	dct:temporal [ a dct:PeriodOfTime;
		schema:startDate "1000-01-01T00:00:00Z"^^xsd:dateTime;
		schema:endDate "1899-12-31Thh:mm:ssZ"^^xsd:dateTime;
	];

	#Organisation
	dct:publisher <ROR:00qps9a02> ;

	#Person role
	dcat:contactPoint <https://orcid.org/0000-0001-6147-9981/author> ;

	#Security constraints - Security of data storage -Security of data transfer- Privacy constraints - Contribution policies
	#dct:accessRights "";

	#Curation and provenance obligations
	#dct:provenance  "";

	#Related publications
	dct:isReferencedBy "https://doi.org/10.5194/essd-14-5213-2022";

	#Additional metadata
	#dcat:landingPage "";

	#Quality assurance
	dqv:hasQualityAnnotation [ a oa:Annotation ;
		oa:hasBody "https://www.emidius.eu/epica/data_quality_assurance.htm"^^xsd:anyURI ;
	];
	#Project ID
	#dct:isReferencedBy
    #Periodicity
	dct:accrualPeriodicity "http://purl.org/cld/freq/irregular"^^xsd:anyURI;
    dcat:theme  <category:DTGEO_conc> ;
.

### Distribution

<https://www.epos-eu.org/epos-dcat-ap/Seismology/Dataset/AHEAD/events/EPICA/distribution/OGC/WMS> a dcat:Distribution ;
	dcat:accessURL "https://www.emidius.eu/epica/data/EPICA_v1.1.xlsx"^^xsd:anyURI;

	dct:identifier "https://www.epos-eu.org/epos-dcat-ap/Seismology/Dataset/AHEAD/events/EPICA/distribution/OGC/WMS" ;

	dct:title "European PreInstrumental Earthquake Catalogue (EPICA)" ;
	#Description
	dct:description "The European Pre-Instrumental Earthquake Catalogue (EPICA) is the 1000-1899 seismic catalogue compiled within the framework of the SERA (Seismology and Earthquake Engineering Research Infrastructure Alliance for Europe) Horizon 2020 project for the European Seismic Hazard Model 2020 (ESHM20).";

	#File format https://www.iana.org/assignments/media-types/media-types.xhtml
	#-->shapefiles, csv
	#dct:format "shapefile"^^xsd:anyURI;
	dcat:mediaType "https://www.iana.org/assignments/media-types/application/csv"^^xsd:anyURI;

	#	URL
	dcat:downloadURL "https://www.emidius.eu/epica/data/EPICA_v1.1.xlsx"^^xsd:anyURI; #for downloadable file
	#dcat:accessService <>;#DataService that gives access to the Dataset

	#Maturity level
	# adms:status

	#Licensing URL - #Licensing constraints (to be renamed to #“Licensing ID”)
	#https://spdx.org/licenses
	dct:license "https://spdx.org/licenses/CC-BY-4.0.html"^^xsd:anyURI;

	#minSTORMB
	#dcat:byteSize "";
.

### Organization

<ROR:00qps9a02> a schema:Organization;
	#mandatory in v3
	schema:identifier [ a schema:PropertyValue ;
		schema:propertyID "ROR" ;
		schema:value "00qps9a02" ;
	];
	schema:identifier [ a schema:PropertyValue ;
		schema:propertyID "PIC" ;
		schema:value "999472675" ;
	];
	#Organisation name
	schema:legalName "INGV - Istituto Nazionale di Geofisica e Vulcanologia";
	schema:contactPoint <https://orcid.org/0000-0001-6147-9981/author> ;
	schema:address [ a schema:PostalAddress ; #ADDED
		schema:streetAddress "Via di Vigna Murata, 605" ;
		schema:addressLocality "Rome" ;
		schema:postalCode "00143" ;
		schema:addressCountry "Italy" ;
	];
	schema:url "http://www.ingv.it"^^xsd:anyURI  ;
	#Organisation role -> publisher of dataset
	#Facility/equipment role
	#Organization schema:owns
.

### Person

<https://orcid.org/0000-0001-6147-9981> a schema:Person;
	schema:identifier [ a schema:PropertyValue;
		schema:propertyID "orcid";
		schema:value "0000-0001-6147-9981";
	];
	schema:familyName "Rovida";
	schema:givenName "Andrea";
	schema:address [ a schema:PostalAddress;
		schema:streetAddress "via Alfonso Corti 12";
		schema:addressLocality "Milano";
		schema:postalCode "20133";
		schema:addressCountry "ITALY";
	];
	schema:email "andrea.rovida@ingv.it";
	schema:telephone "+390223699257";
	schema:qualifications "Researcher";
	schema:affiliation <ROR:00qps9a02> ;
	schema:contactPoint <https://orcid.org/0000-0001-6147-9981/author> ;
.

### ContactPoint

<https://orcid.org/0000-0001-6147-9981/author> a schema:ContactPoint ;
	#Person email
	schema:email "andrea.rovida@ingv.it";
	#Person role
	schema:contactType "author";
	schema:availableLanguage "en" ;
.
